/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.cata.adminprod.controller;

import cl.cata.adminprod.dao.ProductoJpaController;
import cl.cata.adminprod.entity.Producto;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author catal
 */
@WebServlet(name = "EditProductoController", urlPatterns = {"/edit"})
public class EditProductoController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet EditProductoController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet EditProductoController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ProductoJpaController productoDao= new ProductoJpaController();
       String id=request.getParameter("id");
       Producto prod=productoDao.findProducto(Long.parseLong(id));
       request.setAttribute("producto", prod);
       request.getRequestDispatcher("editProductos.jsp").forward(request, response); 
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ProductoJpaController productoDao= new ProductoJpaController();
         String id=request.getParameter("id");
         Producto prod=productoDao.findProducto(Long.parseLong(id));
         if(prod!=null)
         {
            try {
                prod.setCodigo(request.getParameter("codigo"));
                prod.setMarca(request.getParameter("marca"));
                prod.setNombreProducto(request.getParameter("nombre"));
                prod.setStock(Integer.valueOf(request.getParameter("stock")));
                prod.setTipo(request.getParameter("tipo"));
                productoDao.edit(prod);
                
                List<Producto> productos = new ArrayList<>();
                productos=productoDao.findProductoEntities();
                request.setAttribute("productos", productos);
                request.getRequestDispatcher("listadoProductos.jsp").forward(request, response);
            } catch (Exception ex) {
                Logger.getLogger(EditProductoController.class.getName()).log(Level.SEVERE, null, ex);
            }
         }else
         {
             System.out.println("Error no existe producto");
         }
         
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
