<%-- 
    Document   : listadoProductos
    Created on : 05-06-2021, 22:09:23
    Author     : catal
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="cl.cata.adminprod.entity.Producto"%>

<!DOCTYPE html>
<%  List<Producto> productos = (List<Producto>) request.getAttribute("productos");
    if (productos == null) {
        response.sendRedirect("index.jsp");
    }
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
        <script src="https://kit.fontawesome.com/84fe66ef08.js" crossorigin="anonymous"></script>
        <title>Listado de productos</title>
        <style>
            .conteiner
            {
                width: 70%;
                height: 80%;
                margin-top: 10px;
                margin: 5px;
                border-radius: 8px;
                box-shadow:3px 5px #888888;
                border: 1px solid;

            }
            .form-control{
                width: 50%;
            }
            .form1{
                margin-left: 10px;
            }
        </style>
    </head>
    <body class="m-0 vh-100 row justify-content-center aling-item-center">
        <div class="conteiner">
            <div class="row">
                <h3>Listado de productos</h3>
            </div>
            <br>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Codigo</th>
                        <th>Nombre</th>
                        <th>Marca</th>
                        <th>Stock</th>
                        <th>Tipo</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                  <%for(Producto pro: productos) {%>
                    <tr>
                        <td><%= pro.getId()%></td>
                        <td><%= pro.getCodigo() %></td>
                        <td><%= pro.getNombreProducto() %></td>
                        <td><%= pro.getMarca()%></td>
                        <td><%= pro.getStock()%></td>
                        <td><%= pro.getTipo()%></td>
                        <td> <button class="btn btn-info" type="button" onclick="edit(<%= pro.getId()%>)"><i class="fas fa-edit"></i></button><button type="button" class="btn btn-danger" onclick="borrar(<%= pro.getId()%>)"><i class="fas fa-trash-alt"></i></button> </td>
                    </tr>
                   <% }%>
                </tbody>
            </table>
                 <div class="row"><div class="col-sm-4"><button type="button" class="btn btn-info" onclick="getBack()"><i class="fas fa-reply"> volver</i></button></div></div>
        </div>
    </body>
    <script>
        function edit(id)
        {
            window.location="/cata.ciisa-1.0-SNAPSHOT/edit?id="+id;
        }
        
        function borrar(id)
        {
            if(confirm("Desea borrar este producto?"))
            {
                 window.location="/cata.ciisa-1.0-SNAPSHOT/delete?id="+id;
            }
           
        }
         function getBack()
        {
            window.location="./";
        }
    </script>
</html>
