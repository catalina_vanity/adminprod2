<%-- 
    Document   : editProducto
    Created on : 30-05-2021, 20:18:39
    Author     : catal
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="cl.cata.adminprod.entity.Producto"%>

<!DOCTYPE html>
<%  Producto producto = (Producto) request.getAttribute("producto");
    if (producto == null) {
        response.sendRedirect("index.jsp");
    }
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
        <script src="https://kit.fontawesome.com/84fe66ef08.js" crossorigin="anonymous"></script>
        <title>Gestor de productos</title>
        <style>
           .conteiner
           {
               width: 70%;
               height: 80%;
               margin-top: 10px;
               margin: 5px;
               border-radius: 8px;
               box-shadow:3px 5px #888888;
               border: 1px solid;
               
           }
           .form-control{
               width: 50%;
           }
           .form1{
               margin-left: 10px;
           }
        </style>
    </head>
    <body class="m-0 vh-100 row justify-content-center aling-item-center">
        <div class="conteiner">
            <div class="row">
                <h3>Ingreso Producto</h3>
            </div>
            <br/>
            <form id="formProducto" name="formProducto" method="POST" action="/cata.ciisa-1.0-SNAPSHOT/edit" class="form1" >
                <input type="hidden" name="id" id="id" value="<%=producto.getId()%>"/>
               <div class="row">
                   <label for="codigo">Codigo producto</label>
                   <input type="text" maxlength="5" placeholder="cod00" class="form-control" name="codigo" id="codigo" required value="<%=producto.getCodigo()%>">
               </div>
               <div class="row">
                   <label for="nombre">Nombre producto</label>
                   <input type="text" maxlength="50" placeholder="zapatilla patito" class="form-control" name="nombre" id="nombre" required value="<%=producto.getNombreProducto()%>">
               </div>
               <div class="row">
                   <label for="stock">Stock producto</label>
                   <input type="number" min="1" class="form-control" name="stock" id="stock" required value="<%=producto.getStock()%>">
               </div>
                <div class="row">
                   <label for="marca">Marca producto</label>
                   <input type="text" maxlength="50" placeholder="pctronic" class="form-control" name="marca" id="marca" required value="<%=producto.getMarca()%>">
               </div>
                <div class="row">
                   <label for="tipo">Tipo producto</label>
                   <input type="text" min="1" class="form-control" name="tipo" id="tipo" required value="<%=producto.getTipo()%>">
               </div>
                <br/>
                <div class="row">
                    <div class="col-sm-2">
                        <button class="btn btn-success" type="submit"><i class="far fa-save"> Actualizar</i></button>
                    </div>
                    <div class="col-sm-2"><button type="button" class="btn btn-info" onclick="getBack()"><i class="fas fa-reply"> volver</i></button></div>
                </div>
            </form>
          
        </div>
    </body>
    <script>
        function getBack()
        {
            window.location="ListarProductoController";
        }
    </script>
</html>
