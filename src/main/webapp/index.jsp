<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
        <script src="https://kit.fontawesome.com/84fe66ef08.js" crossorigin="anonymous"></script>
        <title>Gestor de productos</title>
        <style>
           .conteiner
           {
               width: 70%;
               height: 80%;
               margin-top: 10px;
               margin: 5px;
               border-radius: 8px;
               box-shadow:3px 5px #888888;
               border: 1px solid;
               
           }
        </style>
    </head>
    <body class="m-0 vh-100 row justify-content-center aling-item-center">
        <div class="conteiner">
            <div class="row">
                <h3>Gestor de productos</h3>
            </div>
            <div class="row">
                <i class="fab fa-product-hunt"> <a href="/cata.ciisa-1.0-SNAPSHOT/CrearProductoController" class="text-decoration-none">Crear Producto</a></i>
            </div>
            <br>
            <div class="row">
                <i class="fas fa-list-ol"><a href="/cata.ciisa-1.0-SNAPSHOT/ListarProductoController" class="text-decoration-none">Ver Productos</a></i>
            </div>
        </div>
    </body>
</html>
